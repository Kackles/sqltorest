package no.noroff.SQLtoREST.controllers;

import no.noroff.SQLtoREST.SqLtoRestApplication;
import no.noroff.SQLtoREST.models.Customer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {

    @GetMapping("/customer/{ID}")
    public Customer customerGet(@PathVariable String ID) {
        System.out.println("Trying to find customer " + ID);
        Customer returnCustomer = null;

        for(Customer cust : SqLtoRestApplication.customerList) {
            if(cust.getId().equals(ID)) {
                System.out.println("--- CUSTOMER FOUND ---");
                returnCustomer = cust;
            }
        }
        if(returnCustomer == null) {
            System.out.println("--- !CUSTOMER NOT FOUND! ---");
        }
        return returnCustomer;
    }
}
