package no.noroff.SQLtoREST.models;

public class Customer {
    private String id;
    private String contactName;
    private String companyName;
    private String contactTitle;
    private String city;
    private String postCode;

    public Customer(String id, String contactName, String companyName, String contactTitle, String city, String postCode) {
        this.id = id;
        this.contactName = contactName;
        this.companyName = companyName;
        this.contactTitle = contactTitle;
        this.city = city;
        this.postCode = postCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }
}
