package no.noroff.SQLtoREST;

import no.noroff.SQLtoREST.models.Customer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;
import java.util.ArrayList;

@SpringBootApplication
public class SqLtoRestApplication {

	private static String url = "jdbc:sqlite:resources/Northwind_small.sqlite";
	private static Connection conn = null;
	public static ArrayList<Customer> customerList = new ArrayList<>();

	public static void main(String[] args) {

		openConn();

		readCustomers();

		closeConn();

		SpringApplication.run(SqLtoRestApplication.class, args);
	}

	static private void openConn() {
		try {
			// db parameters
			//create a connection to the database
			conn = DriverManager.getConnection(url);
			System.out.println("Connection to SQLite has been established.");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (conn == null) {
					conn.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
	}

	static private void closeConn() {
		try {
			conn.close();
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}

	static private void readCustomers() {
		String sql = "SELECT Id, CompanyName, ContactName, ContactTitle, City, PostalCode FROM Customer";
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				customerList.add(new Customer(rs.getString(1), rs.getString(3), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(6)));
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}

}
